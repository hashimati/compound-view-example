package com.example.compoundview;

import android.os.Bundle;
import android.app.Activity;
import android.content.DialogInterface;

import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
public class Custom extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_custom);

		final CustomView custom = (CustomView)findViewById(R.id.Mycustom); 
		Button lbl = (Button)findViewById(R.id.setLabel); 
		Button val = (Button)findViewById(R.id.setValue); 
		
		lbl.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				custom.setLabel("Hello"); 
			}
		}); 
				val.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						custom.setValue("World"); 
					}
				}); 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.custom, menu);
		return true;
	}

}
