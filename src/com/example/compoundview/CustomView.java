package com.example.compoundview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomView extends LinearLayout
{
	TextView lbl; EditText val; 
	public CustomView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.CustomView, 0, 0); 
		String label = a.getString(R.styleable.CustomView_label); 
		String value = a.getString(R.styleable.CustomView_value); 
		a.recycle(); 
		
		setOrientation(LinearLayout.HORIZONTAL); 
		setGravity(Gravity.CENTER_VERTICAL); 
		
		LayoutInflater inflater = (LayoutInflater)context. 
				getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		
		inflater.inflate(R.layout.customui, this, true); 
		
		
		 lbl =(TextView)getChildAt(0); 
		lbl.setText(label); 
		
		 val = (EditText)getChildAt(1); 
		val.setText(value); 
		
	}
	public CustomView(Context context){
		
		this(context, null); 
	}
	public void setLabel(String string)
	{
		
		lbl.setText(string); 
	}
	public void setValue(String string)
	{	
		val.setText(string); 
	}
	

}
